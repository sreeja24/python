def getProduct(num1,num2):
	
	if(num1 <= 0 or num2 <= 0):
		return "-1"
	if(num1 >= 100000 or num2 >= 100000):
		return "-2"
	
	res = ""
	sum1 = 0
	while(num1 > 0):
		if(num1 % 2 != 0):
			sum1 += num2	
			res += str(num2) + "+"
		num1 /= 2
		num2 *= 2 
		
	return res[:-1] + "=" + str(sum1)
 

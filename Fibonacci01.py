def getFibonacciSeries(num):
	n1 = 0
	n2 = 1
	res = str(n1) + "," + str(n2) + "," 
	n3 = 0
	i = 1
	if(num < 1 or num > 40):
		return "-1"

	while( i <= num-2):
				
		n3 = n1 + n2				
		n1 = n2
		n2 = n3	
						
		res += str(n3) + "," 
		i = i + 1

	res = res[:-1]	
	return  (res)






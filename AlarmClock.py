def ringAlarm(dayOfWeek,onVac):
	if(dayOfWeek < 0 or dayOfWeek > 6):
		return "Invalid Inputs"
	result = " "
	
	if(onVac == True):
		if(dayOfWeek > 0 or dayOfWeek < 5):
			result = "10:00"
		if(dayOfWeek == 0 or dayOfWeek == 6):
			result = "OFF"
	if(onVac == False):
		if(dayOfWeek > 0 or dayOfWeek < 5):
			result = "07:00"
		if(dayOfWeek == 0 or dayOfWeek == 6):
			result = "OFF"
	return result	

